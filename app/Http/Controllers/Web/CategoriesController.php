<?php
namespace App\Http\Controllers\Web;
use Illuminate\Http\Request;
use App\Category;
use App\Page;
use App\SiteSetting;
use Illuminate\Database\Eloquent\Builder;
use App\Http\Controllers\Controller;
class CategoriesController extends Controller {
  public function __construct() {
  }
  public function index() {
    $data = [];
    try{
        $siteid = config('app.siteid');
        $data['list'] = Category::CustomWhereBasedData($siteid)->select('id','title')->get()->toArray();
        return view('web.category.index')->with($data);
    }catch (\Exception $e) {
			abort(404);
    }
   
  }
    public function detail() {
        $data = [];
        try{
            $siteid = config('app.siteid');
            $data['detail'] = Category::select('id','title','slug','short_description','long_description','meta_title','meta_keywords','meta_description','sort','parent_id','user_id')->with(['categoryStores'=>function($q1){
                $q1->select('id','name','short_description','long_description','sort','html_tags','script_tags','viewed','meta_title','meta_keywords','meta_description','old_url','new_url')->with('slugs');
            }])->with('sites')->whereHas('sites', function($q) use ($siteid) {
        $q->where('site_id',$siteid);
        } )->with('parentCategories')->where('publish',1)->where('id',PAGE_ID)->first()->toArray();
        if($data['detail'] == null){
            abort(404);
        }

        $data['popular'] = Category::where('publish',1)->with('sites')->whereHas('sites', function($q) use ($siteid) {
        $q->where('site_id',$siteid);
        } )->where('popular',1)->with('slugs')->orderBy('title')->get()->toArray();

        $meta['title']=$data['detail']['meta_title'];
        $meta['keywords']=$data['detail']['meta_keywords'];
        $meta['description']=$data['detail']['meta_description'];
        $data['meta']=$meta;

        return view('web.category.detail')->with($data);
        }catch (\Exception $e) {
                abort(404);
        }
   
    }
}